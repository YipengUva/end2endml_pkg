# dummyML package

The dummyML Python package is designed for data analysts to do automated data analysis on tabular data using machine learning algorithms. The package implemented all the components, data preprocessing, data splitting, model selection, model fitting and model evaluation, required for defining pipelines to automatically analyze tabular data. 

### Installation

Install dummyML package by running:

```
pip install dummyML
```

on the command line of a Linux system or the Anaconda Prompt on a Windows system. If you don't have root privileges, sometimes you need to add `--user` after the above commands, then pip will install the package in your home directory.

If you want to install a specific version, please visit https://pypi.org/project/dummyML/ to find the exact version number. For example, you can install version 0.9.5 by `pip install dummyML==0.9.5`.

### User guide

The user guide is available at https://dummyml.readthedocs.io/en/latest/.
