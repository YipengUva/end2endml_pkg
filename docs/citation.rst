Citation
=======

If you use dummyML in a scientific publication, please use the following citation: Song, Y., Yang, L., Cao, Wang, M., Metes, D., Cao, B., (2024). dummyML (Version 0.9.6) [Software]. Available from https://pypi.org/project/dummyML/.
