Credits
=======

Mengzhe Wang, a manager at Ministry of Health in Alberta, and Dan Metes, a Senior Health System Analyst
at Ministry of Health in Alberta, originally had the idea of developing a machine
learning-based pipeline to do automatic data analysis on their administrative data sets. 
Yipeng, a postdoc at the Computational Psychiatry Lab at the University of
Alberta, volunteered to implement the machine learning parts 
of the pipeline. Then we found the pipeline is very useful to do fast data analysis in 
our research projects. Thus, we decided to implement a Python package for it. 
The package was implemented by Yipeng. The features of the Python package are the 
results of multiple discussions between Yipeng, Mengzhe, Dan, and other team members in Computational Psychiatry Lab.

The project is supported by Dr. Bo Cao, PI of the Computational Psychiatry Lab and the Canada Research Chair in Computational Psychiatry at the University of Alberta (www.ualberta.ca/medicine/about/people/details.html?n=bo-cao), who is supported by the Canada Research Chairs (CRC) program, Alberta Innovates, University Hospital Foundation, Simon & Martina Sochatsky Fund for Mental Health, Mental Health Foundation and University of Alberta.
