Installation
============

Requirements
------------

The dummyML package requires the following dependencies:

.. code:: 

   numpy>=1.19, <=1.26
   pandas>=1.1, <=1.4
   imbalanced-learn>=0.8, <=0.10
   scikit-learn>=1.0, <=1.2
   pandas-profiling>=2.9, <=3.3
   joblib>=1.0, <=1.2
   xgboost>=1.4, <=1.5
   optuna>=2.7, <=2.10


Install
-------

It is recommended to use a virtual environment to install the package and its dependencies even though this step is optional. You can use ``conda create --name your_env_name python=3.9`` to create a virtual environment if Anaconda has already been installed on your computer. Then use ``conda activate your_env_name`` to activate the virtual environment. A quick introduction of using conda to manage the virtual environment can be found at https://codingfordata.com/8-essential-commands-to-get-started-with-conda-environments/.

The package can be installed by running: ``pip install dummyML`` on the terminal of a Linux system or the Anaconda Prompt on a Windows system. If you don't have root privileges, sometimes you need to add ``--user`` after the above commands, then pip will install the package in your home directory.

If you installed the package before, you can use ``pip install dummyML --upgrade`` to update the package to the latest version. You can check the installed package version using ``import dummyML; print(dummyML.__version__)``. If you encounter a problem, you can also uninstall the package using ``pip uninstall dummyML``, then reinstall the package again.
